#!/usr/bin/zsh
function insert-if-missing-at-zshrc {
  LINE_TO_INSERT=$1
  FILE=~/.zshrc
  if ! grep -qFx $LINE_TO_INSERT $FILE ; then
    echo "added $LINE_TO_INSERT to $FILE"
    echo $LINE_TO_INSERT >> $FILE
  fi
}

function install-psql {
	brew install libpq
	insert-if-missing-at-zshrc 'export PATH="/opt/homebrew/opt/libpq/bin:$PATH"'
}

function install-zsh-syntax-highlighting {
  brew install zsh-syntax-highlighting
  export ZSH_HIGHLIGHT_HIGHLIGHTERS_DIR=/opt/homebrew/share/zsh-syntax-highlighting/highlighters
}

function install-brew {
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
	insert-if-missing-at-zshrc "export PATH=$(brew --prefix)/bin:$PATH"
}

function install-nvm {
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
  insert-if-missing-at-zshrc "export NVM_DIR=~/.nvm"
	insert-if-missing-at-zshrc '[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"'
}

function set-ctrlc-to-interrupt-everything {
  trap "exit" INT
}

function install-docker-with-compose {
  sudo pacman -S --noconfirm docker
  sudo gpasswd -a $USER docker
  sudo mkdir -p $DOCKER_CONFIG/cli-plugins
  sudo curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose
  sudo chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose
  sudo systemctl enable --now docker
}

function install-tmuxinator {
  gem install tmuxinator
  insert-if-missing-at-zshrc 'export PATH=$PATH:/home/z1dl/.local/share/gem/ruby/3.0.0/bin'
  insert-if-missing-at-zshrc 'export SHELL=/usr/bin/zsh'
  insert-if-missing-at-zshrc 'export EDITOR=vim'
}

function install-aws-cli {
  INITIAL_DIR=$(pwd)
  cd ~/
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  unzip -q awscliv2.zip
  sudo ./aws/install
  rm awscliv2.zip
  cd $INITIAL_DIR
}
