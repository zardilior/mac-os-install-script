#!/bin/zsh

source load-functions.zsh
set-ctrlc-to-interrupt-everything
install-brew
## browser
brew install --cask brave-browser
# terminal
brew install iterm2
brew install --cask alacritty
## developer tools
install-psql
brew install tmux
brew install tmuxinator
brew install fzf
/opt/homebrew/opt/fzf/install
brew install ripgrep
brew install jq 
brew install broot 
broot --install
brew install tldr
brew install difftastic
brew install bat
softwareupdate --install-rosetta
brew install --cask docker 
brew install docker 
brew install --overwrite docker
brew install --cask postman
brew install docker-compose 
install-zsh-syntax-highlighting
#set-fzf-bindings-and-completion
### cloud CLIs
#install-aws-cli
brew install awscli
### package managers
brew install npm
brew install yarn
brew install rustup 
### version managers
install-nvm
## load new zsh config
source ~/.zshrc
./check-binaries.zsh
