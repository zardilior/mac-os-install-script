#!/bin/zsh
# load new zsh config
source ~/.zshrc 
brew --version | head -n 1
vim --version | head -1
unzip -v | head -1
alacritty -V
## developer tools
psql --version
tmux -V
tmuxinator version
echo "fzf $(fzf --version)"
rg  --version | head -1
jq --version
broot --version
tldr --version
difft --version
bat --version
docker --version
docker-compose version
## TODO: postman --version
git --version
### cloud CLIs
aws --version
### package managers
echo "npm $(npm --version)"
echo "yarn $(yarn --version)"
rustup-init  --version
### version managers
echo "nvm $(nvm --version)"
